import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native';
import Formulario from './components/Formulario';
import Clima from './components/Clima';

export default function App() {

  const [ busqueda, setBusqueda ] = useState({
    pais: '',
    ciudad: ''
  });

  const [ consultar, setConsultar ] = useState(false);
  const [ resultado, setResultado ] = useState({});
  const [ bgColor, setBgColor ] = useState('rgb(71, 149, 212)');

  const {pais, ciudad } = busqueda;


  useEffect(() => {
   const consultarClima = async () => {
    if(consultar){
      const apiId = 'a88fbb2d1b8491261bd6bf01bd6dcd15'
      const url = `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${apiId}`

      try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        setResultado(resultado);
        setConsultar(false);

        // Modifica el color de fondo de acuerdo a la temperatura

        const kelvin = 273.15;
        const { main } = resultado;
        const actual = main.temp - kelvin;

        if(actual < 10 ){
          setBgColor('rgb( 105, 108, 149)');
        }else if(actual >= 10 && actual < 25){
          setBgColor('rgb(71, 149, 212)');
        }else {
          setBgColor('rgb(178, 28, 61)');
        }

      } catch (error) {

        mostrarAlerta();
        
      }
    }
   }
   consultarClima();
  }, [consultar]);

  const mostrarAlerta = () => {
    Alert.alert(
        'Error',

        'No hay resultado, intenta con otra cuidad o pais',
        [{
            text: 'OK'
        }]
    )
}

  const ocultarTeclado = () => {
    Keyboard.dismiss();
  }

  const bgColorApp = {
    backgroundColor: bgColor
  }

  return (
    <>
      <TouchableWithoutFeedback onPress={() => ocultarTeclado()} >
        <View style={[styles.app, bgColorApp]}>
          <View style={styles.container}>
            <Clima resultado={resultado}/>
            <Formulario
              busqueda={busqueda}
              setBusqueda={setBusqueda}
              setConsultar={setConsultar}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
    justifyContent: 'center'
  },
  container: {
    marginHorizontal: '2.5%'
  }
});
