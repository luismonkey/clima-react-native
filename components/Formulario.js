import React, { useState } from 'react';
import { Text, View, TextInput, StyleSheet, Picker, TouchableWithoutFeedback, Animated, Alert } from 'react-native';



const Formulario = ({ busqueda, setBusqueda, setConsultar }) => {

    //Extraemos pais y cuidad de la busqueda
    const {pais, ciudad} = busqueda;

    //state para las animaciones, el cual solo se modifica con la API de Animated
    //Por eso no tiene un segundo parémetro tipo setAnimacion
    // el 1 (100%) significa el tamaño, la escala
    const [ animacionBtn ] = useState(new Animated.Value(1));

    const consultarClima = () => {
        if(pais.trim() === '' || ciudad.trim() === ''){
            mostrarAlerta();
            return;
        }

        //pasada la validación, consultar la API
        setConsultar(true)
    }

    const mostrarAlerta = () => {
        Alert.alert(
            'Error',

            'Add country and city',
            [{
                text: 'OK'
            }]
        )
    }

    const animatedInput = () => {
        //se usa un efecto de los que brinda Animated y se le pasa por parametro el state
        //se le pasa por parametro el state al cual hará referencia 
        //y un arreglo de configuración
        Animated.spring( animacionBtn, {
            //en este caso el btn cambiara de su tamaño actual que es 1 a .9 con animación
            toValue: .75
        } ).start();
    }

    const animatedOutput = () => {
        Animated.spring( animacionBtn, {
            //en este caso el btn cambiara de su tamaño actual que es 1 a .9 con animación
            toValue: 1,
            friction: 3,
            tension: 30
        } ).start();

    }

    //declaramos un objeto
    const styleAnimated = {
        //scale es una propiedad que permitar cambiar tamaño de elementos
        transform: [{ scale: animacionBtn }]
    }

    return (
        <>

            <View style={styles.form}>
                
                <View style={{ backgroundColor: '#FFF' }}>
                    <Picker selectedValue={pais}
                        onValueChange={ pais => setBusqueda({ ...busqueda, pais})}>

                        <Picker.Item label="-- Select a country --" value="" />
                        <Picker.Item label="Estados Unidos" value="US" />
                        <Picker.Item label="México" value="MX" />
                        <Picker.Item label="Argentina" value="AR" />
                        <Picker.Item label="Colombia" value="CO" />
                        <Picker.Item label="Venezuela" value="VE" />
                    </Picker>
                </View>

                <View>
                    <TextInput
                        value={ciudad}
                        placeholder="City"
                        onChangeText={ ciudad => setBusqueda({ ...busqueda, ciudad})}
                        placeholderTextColor="#666"
                        style={styles.input}
                    />
                </View>

                <TouchableWithoutFeedback 
                    //Cuando se presiona
                    onPressIn={() => animatedInput()}
                    //cuando sueltas
                    onPressOut={() =>  animatedOutput()}

                    onPress={() => consultarClima()}
                >
                    <Animated.View 
                        style={[styles.btnBuscar, styleAnimated]} 
                    >
                        <Text style={styles.txtBuscar}>Search Weather</Text>
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>

        </>
    );
}

const styles = StyleSheet.create({
    input: {
        padding: 10,
        height: 50,
        backgroundColor: "#FFF",
        fontSize: 20,
        marginTop: 20,
        textAlign: 'center'
    },
    btnBuscar: {
        marginTop: 20,
        backgroundColor: '#000',
        padding: 10,
        justifyContent: 'center'
    },
    txtBuscar: {
        color: '#FFF',
        textAlign: 'center',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        fontSize: 18
    }
})

export default Formulario;
